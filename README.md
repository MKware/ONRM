# ONRM
Oversimplified Number Randomizing Machine.

*Despite it's name, it's really simple*

[![Project Status: Moved to https://codeberg.org/MKware/MKRS – The project has been moved to a new location, and the version at that location should be considered authoritative.](https://www.repostatus.org/badges/latest/moved.svg)](https://www.repostatus.org/#moved) to [https://codeberg.org/MKware/MKRS](https://codeberg.org/MKware/MKRS)


The name says it all - randomizer with configurable range.

